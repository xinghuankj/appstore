应用名称: 7dtd-server
版本号: latest
图标地址: https://www.easypc.io/img/game-hosts/7-days/game.jpg
应用描述: 7日杀游戏服务器（注意这个容器只支持x64架构，要求20G硬盘空间与4G内存空间）配置这个需要一定 Linux 基础，项目地址：https://github.com/Didstopia/7dtd-server  修改服务器配置参考这个文档：https://www.cnblogs.com/shiraka/p/14004966.html
分组名称: 456