# 应用信息
``` md
应用名称: Factorio
版本号: latest
图标地址: https://factorio.com/static/img/factorio-wheel.png
应用描述: 「异星工厂」游戏服务端
分组名称: 123
```


# compose yml格式
``` yml
version: '2.1'
services:
  Factorio:
    image: factoriotools/factorio:latest
    container_name: Factorio
    restart: unless-stopped
    environment:
      TZ: Asia/Shanghai
    volumes:
      - config:/factorio
    ports:
      - "27015:27015/tcp"
      - "34197:34197/udp"
volumes:
  config:
    external: false

```

# 应用备注 yaml格式

``` yaml
note:
  Factorio: 
    environment: 
      TZ:Asia/Shanghai: 时区
    volumes: 
      config:/factorio: 配置文件
    ports: 
      27015:27015/tcp: TCP端口
      34197:34197/udp: UDP端口
```
