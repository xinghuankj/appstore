应用名称: Homebox
版本号: latest
图标地址: https://cdn.jsdelivr.net/gh/PapirusDevelopmentTeam/papirus_icons/src/apps_speedtest.svg
应用描述: 家庭网络工具箱，主要用于组建家庭局域网时的一些调试、检测、压测工具
分组名称: 网络
