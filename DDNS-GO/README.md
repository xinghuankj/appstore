应用名称: DDNS-GO
版本号: latest
图标地址: https://www.hiltron.it/security/wp-content/uploads/sites/3/2020/12/DDNS.png
应用描述: 自动获得你的公网 IPv4 或 IPv6 地址，并解析到对应的域名服务。
分组名称: 123
