# 应用信息
``` md
应用名称: emby
版本号: latest
图标地址: https://cdn.jsdelivr.net/gh/MediaBrowser/Emby/Emby.Dlna/Images/logo240.png
应用描述: 一款成熟强大的影音媒体中心服务器软件
分组名称: 123
```


# compose yml格式
``` yml
version: '2.1'
services:
  emby:
    image: linuxserver/emby:latest
    container_name: emby
    restart: unless-stopped
    environment:
      TZ: Asia/Shanghai
      PUID: 1000
      PGID: 100
    volumes:
      - config:/config
      - /mnt:/mnt
    ports:
      - "8096:8096/tcp"
volumes:
  config:
    external: false

```
# 应用备注 yaml格式

``` yaml
note:
  emby:
    environment: 
      TZ:Asia/Shanghai: 时区
      PUID:1000: 用户权限UID
      PGID:100: 用户权限GID
    volumes: 
      config:/config: 配置文件
      /mnt:/mnt: 影音文件
    ports: 
      8096:8096/tcp: 访问端口
```
