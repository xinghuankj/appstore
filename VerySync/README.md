# 应用信息
``` md
应用名称: VerySync
版本号: latest
图标地址: https://avatars.githubusercontent.com/u/32695897?v=4
应用描述: 微力同步（VerySync）国人开发简单易用的多平台文件同步软件
分组名称: 123
```


# compose yml格式
``` yml
version: '2.1'
services:
  VerySync:
    image: jonnyan404/verysync:latest
    container_name: VerySync
    restart: unless-stopped
    volumes:
      - config:/data
      - /mnt:/mnt
    ports:
      - "8886:8886/tcp"
      - "22330:22330/tcp"
      - "22330:22330/udp"
volumes:
  config:
    external: false

```
# 应用备注 yaml格式

``` yaml
note:
  VerySync:
    ports:
      8886:8886/tcp: 访问端口
      22330:22330/tcp: 数据传输端口TCP
      22330:22330/udp: 数据传输端口UDP
    volumes:
      config:/data: 配置文件
      /mnt:/mnt: 数据文件    
```
