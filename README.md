# 开源应用商店模板
## 这是fastosdocker应用商店的模板大家有什么推荐应用可以直接分支开pr

##### pr教程：https://blog.51cto.com/Laccoliths/5579901
##### gitee地址：https://gitee.com/xinghuankj/appstore

----------------------------------------------------------------------
## 模板信息
* 包含三部分部分（应用信息，模板配置，应用logo）
* 包含一个文件夹用来表示应用名称
* README用来放应用信息以及注释
* yaml用来放运行yaml
* logo用来放logo最大不超过50k
* 如果有多个版本，1.5.0.0 2.5.0.0 用yaml文件名区分多个版本，在md文件描述-版本里面选择多个版本,逗号区分
```
├── 导航应用
│   ├── 1.5.0.0.yaml
│   ├── 2.5.0.0.yaml
│   ├── README.md
│   └── logo.svg
```
* 所有信息以导航应用模板为准，遇到问题提交iss

```down
应用名称: heimdall  
图标地址: ./logo.png
标题: Heimdall一个强大的导航页 
应用描述: heimdall一个强大的导航页  
作者信息：xxx
应用类型: 123
官网：xxx
文档：没有留空即可
GitHub：https://github.com/linuxsuren/heimdall
```

> 统一使用英文冒号，加空格


## Star History

[![Star History Chart](https://api.star-history.com/svg?repos=https:/&type=Date)](https://star-history.com/#https:/&Date)