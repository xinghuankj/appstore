# 应用信息
``` md
应用名称: Plex
版本号: latest
图标地址: https://cdn.jsdelivr.net/gh/plexinc/pms-docker/img/plex-server.png
应用描述: 老牌影音媒体中心服务器软件
分组名称: 123
```


# compose yml格式
``` yml
version: '2.1'
services:
  Plex:
    image: linuxserver/plex:latest
    container_name: Plex
    restart: unless-stopped
    environment:
      TZ: Asia/Shanghai
      PUID: 1000
      PGID: 100
    volumes:
      - config:/config
      - /mnt:/mnt
    ports:
      - "32400:32400/tcp"
volumes:
  config:
    external: false

```
# 应用备注 yaml格式

``` yaml
note:
  Plex:
    environment:
      Z:Asia/Shanghai: 时区
      PUID:1000: 用户权限UID
      PGID:100: 用户权限GID
    volumes:
      config:/config: 配置文件
      /mnt:/mnt: 影音文件
    ports:
      32400:32400/tcp: 访问端口
```
