# 应用信息
``` md
应用名称: FreshRSS
版本号: latest
图标地址: https://freshrss.org/images/icon.svg
应用描述: 一款开源免费的RSS订阅器
分组名称: 123
```


# compose yml格式
``` yml
version: '2.1'
services:
  FreshRSS:
    image: linuxserver/freshrss:latest
    container_name: FreshRSS
    restart: unless-stopped
    environment:
      TZ: Asia/Shanghai
    volumes:
      - config:/config
    ports:
      - "80:8001/tcp"
       
volumes:
  config:
    external: false

```
# 应用备注 yaml格式

``` yaml
note:
  FreshRSS: 
    environment:
      TZ:Asia/Shanghai: 时区
    volumes: 
      config:/config: 配置文件
    ports: 
      80:8001/tcp: 访问端口
```
