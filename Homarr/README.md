# 应用信息
``` md
应用名称: Homarr
版本号: latest
图标地址: https://homarr.dev/img/logo.svg
应用描述: 一个轻量级多功能导航页
分组名称: 123
```


# compose yml格式
``` yml
version: '2.1'
services:
  Homarr:
    image: ghcr.dockerproxy.com/ajnart/homarr:latest
    container_name: Homarr
    restart: unless-stopped
    environment:
      TZ: Asia/Shanghai
    volumes:
      - config:/app/data/configs
    ports:
      - "7575:7575/tcp"
volumes:
  config:
    external: false

```
# 应用备注 yaml格式

``` yaml
note:
  Homarr: 
    environment:
      TZ:Asia/Shanghai: 时区
    volumes: 
      config:/app/data/configs: 配置文件
    ports: 
      7575:7575/tcp: 访问端口
```
