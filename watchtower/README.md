应用名称: watchtower  
版本号: latest
图标地址: ./image.png
标题: 一种基于容器的解决方案，用于自动执行 Docker 容器基础映像更新。 
应用描述: 一种基于容器的解决方案，用于自动执行 Docker 容器基础映像更新。  
作者信息: containrrr  
应用类型: 容器管理  
官网: [watchtower](https://containrrr.dev/watchtower/)  
文档: 没有留空即可  
GitHub: https://github.com/containrrr/watchtower