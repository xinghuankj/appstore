# 应用信息
``` md
应用名称: FileBrowser
版本号: latest
图标地址: https://cdn.jsdelivr.net/gh/filebrowser/filebrowser/frontend/public/img/logo.svg
应用描述: 一款轻量级网页版文件管理器（默认用户密码都是 admin）
分组名称: 123
```


# compose yml格式
``` yml
version: '2.1'
services:
  FileBrowser:
    image: hurlenko/filebrowser:latest
    container_name: FileBrowser
    user: root
    restart: unless-stopped
    volumes:
      - config:/config
      - /mnt:/data
    ports:
      - "8080:8005/tcp"
volumes:
  config:
    external: false

```
# 应用备注 yaml格式

``` yaml
note: 
  FileBrowser:
    volumes: 
      config:/config: 配置文件
      /mnt:/data: 数据文件
    ports: 
      8080:8005/tcp: 访问端口
```
