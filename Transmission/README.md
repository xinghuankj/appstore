# 应用信息
``` md
应用名称: Transmission
版本号: latest
图标地址: https://cdn.jsdelivr.net/gh/walkxcode/dashboard-icons/png/transmission.png
应用描述: 一款资源占用低的跨平台BitTorrent下载工具
分组名称: 123
```


# compose yml格式
``` yml
version: '2.1'
services:
  Transmission:
    image: linuxserver/transmission:latest
    container_name: Transmission
    restart: unless-stopped
    environment:
      TZ: Asia/Shanghai
      PUID: 1000
      PGID: 100
    volumes:
      - config:/config
      - /mnt:/downloads
    ports:
      - "9091:9091/tcp"
      - "51413:51413/tcp"
      - "51413:51413/udp"
volumes:
  config:
    external: false

```
# 应用备注 yaml格式

``` yaml
note:
  Transmission:
    environment:      
      TZ:Asia/Shanghai: 时区
      PUID:1000: 用户权限UID
      PGID:100: 用户权限GID
    volumes:
      config:/config: 配置文件
      /mnt:/downloads: 下载目录
    ports:
      9091:9091/tcp: web访问端口
      51413:51413/tcp: P2P数据传输端口TCP
      51413:51413/udp: P2P数据传输端口UDP
```
