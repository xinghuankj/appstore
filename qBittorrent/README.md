# 应用信息
``` md
应用名称: qBittorrent
版本号: latest
图标地址: https://cdn.jsdelivr.net/gh/walkxcode/dashboard-icons/svg/qbittorrent.svg
应用描述: 一款功能丰富成熟且高性能的夸平台BitTorrent下载工具（默认账号密码 admin/adminadmin）
分组名称: 123
```


# compose yml格式
``` yml
version: '2.1'
services:
  qBittorrent:
    image: linuxserver/qbittorrent:latest
    container_name: qBittorrent
    restart: unless-stopped
    environment:
      TZ: Asia/Shanghai
      PUID: 1000
      PGID: 100
      WEBUI_PORT: 9092
    volumes:
      - config:/config
      - /mnt:/downloads
    ports:
      - "9092:9092/tcp"
      - "6881:6881/tcp"
      - "6881:6881/udp"
volumes:
  config:
    external: false

```
# 应用备注 yaml格式

``` yaml
note:
  qBittorrent:
    environment:
      TZ:Asia/Shanghai: 时区
      PUID:1000: 用户权限UID
      PGID:100: 用户权限GID
      WEBUI_PORT:9092: web访问端口
    volumes: 
      config:/config: 配置文件
      /mnt:/downloads: 下载目录
    ports:
      9092:9092/tcp: web访问端口
      6881:6881/tcp: P2P数据传输端口TCP
      6881:6881/udp: P2P数据传输端口UDP
```
