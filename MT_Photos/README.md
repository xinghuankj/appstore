# 应用信息
``` md
应用名称: MT_Photos
版本号: latest
图标地址: https://cdn.mtmt.tech/logo.png
应用描述: 一款为Nas玩家准备的照片管理系统,支持自动整理、分类您的照片，比如：时间、地点、人物、照片类型。您可以在任何支持Docker的系统中运行它。（如果是Arm V8架构，请拉取 mtphotos/mt-photos:arm-latest 镜像）
分组名称: 123
```


# compose yml格式
``` yml
version: '2.1'
services:
  MT_Photos:
    image: mtphotos/mt-photos:latest
    container_name: MT_Photos
    restart: unless-stopped
    environment:
      TZ: Asia/Shanghai
    volumes:
      - config:/config
      - /mnt/MT_Photos:/upload
    ports:
      - "8063:8063/tcp"
volumes:
  config:
    external: false

```
# 应用备注 yaml格式

``` yaml
note:
  MT_Photos:
    environment: 
      TZ:Asia/Shanghai: 时区
    volumes:
      config:/config: 配置文件
      /mnt/MT_Photos:/upload: 相册文件
    ports:
      8063:8063/tcp: 访问端口
```
