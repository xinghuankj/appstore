# 应用信息
``` md
应用名称: Squoosh
版本号: latest
图标地址: https://cdn.jsdelivr.net/gh/GoogleChromeLabs/squoosh@1.12.0/src/components/intro/imgs/logo.svg
应用描述: 由谷歌推出的一款在线图像压缩工具，可帮助网站开发人员快速压缩图片，在保持图片质量的同时，提升网站访问速度。
分组名称: 123
```


# compose yml格式
``` yml
version: '2.1'
services:
  Squoosh:
    image: dko0/squoosh:latest
    container_name: Squoosh
    restart: unless-stopped
    ports:
      - "8080:8004/tcp"

```
# 应用备注 yaml格式
``` yaml
note:
  Squoosh:
    ports: 
      8080:8004/tcp: 访问端口
```
