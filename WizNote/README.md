# 应用信息
``` md
应用名称: WizNote
版本号: latest
图标地址: https://cdn.jsdelivr.net/gh/altairwei/WizNotePlus@2.11.3/resources/logo_512.png
应用描述: 为知笔记(WizNote)是一款云服务的笔记软件，也是一款基于笔记的团队协作工具。默认管理员账号：'admin@wiz.cn' 密码：'123456'
分组名称: 123
```


# compose yml格式
``` yml
version: '2.1'
services:
  WizNote:
    image: wiznote/wizserver:latest
    container_name: WizNote
    restart: unless-stopped
    volumes:
      - /etc/localtime:/etc/localtime
      - config:/wiz/storage
    ports:
      - "80:8003/tcp"
volumes:
  config:
    external: false

```
# 应用备注 yaml格式

``` yaml
note:
  WizNote:
    volumes:
      /etc/localtime:/etc/localtime: 设置时区
      config:/wiz/storage: 配置文件
    ports:
      80:8003/tcp: 访问端口
```
