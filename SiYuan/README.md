# 应用信息
``` md
应用名称: SiYuan
版本号: latest
图标地址: https://cdn.jsdelivr.net/gh/siyuan-note/siyuan@2.9.4/app/stage/icon-large.png
应用描述: 思源笔记 (SiYuan) 是一款开源免费且“本地优先”的下一代个人知识管理系统 (笔记软件)
分组名称: 123
```


# compose yml格式
``` yml
version: '2.1'
services:
  SiYuan:
    image: b3log/siyuan:latest
    container_name: SiYuan
    restart: unless-stopped
    user: root
    environment:
      LANGUAGE: zh_CN.UTF-8
    volumes:
      - config:/root/SiYuan
    ports:
      - "6806:6806/tcp"
volumes:
  config:
    external: false

```
# 应用备注 yaml格式

``` yaml
note:
  SiYuan:
    environment:
      LANGUAGE:zh_CN.UTF-8: 软件语言
    volumes:
      config:/root/SiYuan: 配置文件
    ports: 
      6806:6806/tcp: 访问端口
```
