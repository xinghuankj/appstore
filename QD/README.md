# 应用信息
``` md
应用名称: QD
版本号: latest
图标地址: https://qd-today.github.io/qd/logo.png
应用描述: 开源的自动化网站签到框架，国人开发支持众多国内网站
分组名称: 123
```


# compose yml格式
``` yml
version: '2.1'
services:
  QD:
    image: a76yyyy/qiandao:latest
    container_name: QD
    restart: unless-stopped
    volumes:
      - config:/usr/src/app/config
    ports:
      - "80:8002/tcp"
volumes:
  config:
    external: false

```
# 应用备注 yaml格式

``` yaml
note:
  QD:
    volumes:
      config:/usr/src/app/config: 配置文件
    ports:
      80:8002/tcp: 访问端口
```
