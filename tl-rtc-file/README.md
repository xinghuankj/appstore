# 应用信息
``` md
应用名称: tl-rtc-file
版本号: latest
图标地址: https://cdn.jsdelivr.net/gh/tl-open-source/tl-rtc-file/svr/res/image/44826979.png
应用描述: （tl webrtc datachannel filetools）用webrt在web端传输文件，支持传输超大文件。 分片传输，跨终端，不限平台，方便使用，内网不限速（局域网最高到过70多M/s），支持私有部署，支持多文件拖拽发送，网页文件预览
分组名称: 123
```


# compose yml格式
``` yml
version: '2.1'
services:

  tl-rtc-file-api-local:
    image: iamtsm/tl-rtc-file-api-local:latest
    container_name: tl-rtc-file-api-local
    restart: unless-stopped
    command: localapi
    environment:
      WS_HOST: 'ws://127.0.0.1:8444'
      ENV_MODE: 'local'
    ports:
      - "9082:9092/tcp"

  tl-rtc-file-socket-local:
    image: iamtsm/tl-rtc-file-socket-local:latest
    container_name: tl-rtc-file-socket-local
    restart: unless-stopped
    command: localsocket
    environment:
      WS_HOST: 'ws://127.0.0.1:8444'
      ENV_MODE: 'local'
    ports:
      - "8444:8444/tcp"

```
# 应用备注 yaml格式

``` yaml
note:
  tl-rtc-file-api-local:
    environment:
      WS_HOST:'ws://127.0.0.1:8444': WS地址需要按实际修改
      ENV_MODE:'local': 运行模式
    ports:
      9082:9092/tcp: 访问端口
  tl-rtc-file-socket-local:
    environment:
      WS_HOST:'ws://127.0.0.1:8444': WS地址需要按实际修改
      ENV_MODE:'local': 运行模式
    ports:
      8444:8444/tcp: 访问端口
```
