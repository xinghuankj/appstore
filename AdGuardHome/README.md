应用名称: AdGuardHome
版本号: latest
图标地址: https://cdn.jsdelivr.net/gh/walkxcode/dashboard-icons/svg/adguard-home.svg
应用描述: 一款全广告拦截与反跟踪软件。(该软件工作端口众多，极易与其他系统服务冲突。默认只映射 3000 端口用于访问软件设置页面,其他端口按需自行设置。)
分组名称: 123
